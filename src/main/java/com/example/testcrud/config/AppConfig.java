package com.example.testcrud.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.logging.Logger;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.example.testcrud")
public class AppConfig {

    @Autowired
    private Environment env;
    private final Logger logger = Logger.getLogger(getClass().getName());


}
