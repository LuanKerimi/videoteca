package com.example.testcrud.config;

import com.example.testcrud.controller.AppController;
import com.example.testcrud.service.UserService;
import com.example.testcrud.utilClass.AuthenticationSuccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.util.logging.Logger;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserService userService;

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    AuthenticationSuccessHandler customAuthenticationSuccess() {
        return new AuthenticationSuccess();
    }


    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth, HttpSession session) throws Exception {

        User.UserBuilder user = User.withDefaultPasswordEncoder();


        try {
            auth.inMemoryAuthentication()
                    .withUser(user.username("admin")
//                        .passwordEncoder( p -> encoder() + p)
//                        .password("admin").passwordEncoder(p->encoder() + p)
                            .password("admin").roles("ADMIN"))
                    .withUser(user.username("nUser").password("nUser").roles("USER"));

            auth.jdbcAuthentication().dataSource(dataSource)
                    .usersByUsernameQuery("select username, password, enabled from user where username=?")
                    .authoritiesByUsernameQuery("select username, ruolo from user where username=?");

            _log.info("login riuscito");

        } catch (Exception e) {

            _log.info("Login non riuscito");
        }


    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {


        http.authorizeRequests()
                .antMatchers("/user/register", "/home", "/user/save").permitAll()
                .antMatchers("/user/management", "genere/management", "genere/create").hasAuthority("ROLE_ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/authenticateTheUser")
                .successHandler(customAuthenticationSuccess())
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login");

    }



    @Bean
    public HttpFirewall getHttpFirewall() {
        return new DefaultHttpFirewall();
    }


    private static final Logger _log = Logger.getLogger(
            SecurityConfig.class.getName());
}
