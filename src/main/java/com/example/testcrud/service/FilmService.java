package com.example.testcrud.service;


import com.example.testcrud.entity.Film;
import com.example.testcrud.entity.FilmApi;
import com.example.testcrud.repository.FilmRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;

@Service
public class FilmService {
    private final FilmRepository filmRepository;

    @Autowired
    private GenereService genereService;

    @Autowired

    public FilmService(FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    public Film save(Film film) {
        filmRepository.save(film);
        return film;
    }

    public Iterable<Film> findAll() {
        return filmRepository.findAll();
    }

    ;

    public Film findById(long filmId) {
        Film film = filmRepository.findById(filmId).orElseThrow();
        return film;
    }

    public void deleteById(long id) {
        filmRepository.deleteById(id);
    }

    public String getImgData(byte[] byteData) {
        return Base64.getMimeEncoder().encodeToString(byteData);
    }

    public List<FilmApi> getFilmApi() throws IOException {

        ObjectMapper jsonReader = new ObjectMapper();
        JsonNode films = jsonReader.readTree(new URL("https://imdb-api.com/en/API/MostPopularMovies/k_vh6xjdcy"))
                .get("items");
        List<FilmApi> filmApiList = new ArrayList<>();

        int count = 0;
        for (JsonNode en : films) {
            if (count == 6)
                break;
            FilmApi film = new FilmApi();
            film.setTitle(en.get("title").toString().replace("\"", ""));
            film.setYear(en.get("year").toString().replace("\"", ""));
            film.setImage(en.get("image").toString().replace("\"", ""));
            filmApiList.add(film);
            count++;
            System.out.println(film.getImage());

        }
        return filmApiList;
    }

    public List<Film> ordinaAZ() {
        List<Film> filmList = (List<Film>) this.findAll();
        filmList.sort(Comparator.comparing(Film::getTitolo));
        return filmList;
    }

    public List<Film> ordinaZA() {
        List<Film> filmList = (List<Film>) this.findAll();
        filmList.sort(Comparator.comparing(Film::getTitolo).reversed());
        return filmList;
    }

    public List<Film> ordinaAnnoAsc() {
        List<Film> filmList = (List<Film>) this.findAll();
        filmList.sort(Comparator.comparing(Film::getAnno));
        return filmList;
    }

    public List<Film> ordinaAnnoDesc() {
        List<Film> filmList = (List<Film>) this.findAll();
        filmList.sort(Comparator.comparing(Film::getAnno).reversed());
        return filmList;
    }




}



