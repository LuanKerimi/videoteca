package com.example.testcrud.service;


import com.example.testcrud.entity.Film;
import com.example.testcrud.entity.User;
import com.example.testcrud.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.logging.Logger;

@Service
public class UserService {
    private static final Logger _log = Logger.getLogger(UserService.class.getName());
    private final UserRepository userRepository;

//    public User save(User user){userRepository.save(user);
//        return user;
//    }


    @Autowired

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User save(User user) {
        userRepository.save(user);
        _log.info("utente salvato" + user);
        return user;
    }


    public User findByUsernameAndPassword(String username, String password) {
        User user = userRepository.findByUsername(username);
        if (user.getPassword().matches(password)) {
            _log.info("Utente trovato");
            return user;
        }
        _log.info("Utente non trovato");

        return null;
    }

    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }


    public User findById(long userId) {
        User user = userRepository.findById(userId).orElseThrow();
        return user;
    }

    public void deleteById(long id) {
        userRepository.deleteById(id);
    }


    public User getUser(String username, String psw) {
        User user = userRepository.findByEmail(username);
        if (user.getEmail().matches(username) && user.getPassword().matches(psw)) {
            System.out.println("Utente trovato correttamente.");
            return user;
        }
        System.out.println("Utente non trovato.");
        return null;
    }


    public User findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return user;
    }


}



