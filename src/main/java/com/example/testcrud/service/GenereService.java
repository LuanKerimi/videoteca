package com.example.testcrud.service;


import com.example.testcrud.entity.Genere;
import com.example.testcrud.entity.User;
import com.example.testcrud.repository.GenereRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class GenereService {

    @Autowired
    private final GenereRepository genereRepository;

    @Autowired
    public GenereService(GenereRepository genereRepository) {
        this.genereRepository = genereRepository;
    }

    public Iterable<Genere> generiList() {
        return genereRepository.findAll();
    }

    public Genere save(Genere genere) {
        genereRepository.save(genere);
        return genere;
    }

    public ModelAndView generiIndex() {
        Iterable<Genere> generiList = genereRepository.findAll();
        ModelAndView mv = new ModelAndView();
//        mv.setViewName("genereList");
        mv.addObject("generiList", generiList);
        return mv;
    }

    public Genere findById(long genereId) {
        Genere genere = genereRepository.findById(genereId).orElseThrow();
        return genere;
    }

    public void deleteById(long id) {
        genereRepository.deleteById(id);
    }

    public ArrayList<Genere> checkGenere() {
        Iterable<Genere> list = genereRepository.findAll();
        ArrayList<Genere> list2 = new ArrayList<Genere>();
        list2.addAll((Collection<? extends Genere>) list);

        return list2;
    }




}

