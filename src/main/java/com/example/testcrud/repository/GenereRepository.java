package com.example.testcrud.repository;


import com.example.testcrud.entity.Genere;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenereRepository extends CrudRepository<Genere,Long> {

}
