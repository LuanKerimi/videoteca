package com.example.testcrud.entity;


import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;


@Getter
@Setter
@Entity
public class Film {
    @Autowired
    @Id
    @GeneratedValue
    private long id;
    private String titolo;
    private String regista;
    private String anno;
    private Boolean vm18;

    @Lob
    private String immagine;

    @ManyToOne
    private Genere genere;

    public Genere getGenere() {
        return genere;
    }

    public String getGenereName() {
        return this.genere.nomeGenere;
    }


//
//    public void setImmagine(MultipartFile multipartFile) throws IOException {
//
//        File convFile = new File(multipartFile.getOriginalFilename());
//        convFile.createNewFile();
//        FileOutputStream fos = new FileOutputStream(convFile);
//        fos.write(multipartFile.getBytes());
//        fos.close();
//
//        this.immagine = convFile;
//
//    }


}






