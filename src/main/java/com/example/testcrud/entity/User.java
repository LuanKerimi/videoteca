package com.example.testcrud.entity;



import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.validation.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

@Getter
@Setter
@Entity
public class User implements Serializable, UserDetails {
    @Autowired
    @Id
    @GeneratedValue
    private long id=0;
    @NotNull()
    @NotEmpty
    private String username;
    @NotNull()
    @NotEmpty
    private String password;
    @NotNull()
    @NotEmpty
    private String email;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @NotEmpty
    @NotNull
    private String dataDiNascita;
    private boolean enabled = true;
    private String ruolo = "ROLE_USER";
    private String test = "ok";

    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    public void setPassword(String password) {
//       this.password = password;
       this.password = encoder().encode(password);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList(new SimpleGrantedAuthority(this.ruolo));
    }


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

}

