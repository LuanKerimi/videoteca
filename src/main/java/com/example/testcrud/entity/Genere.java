package com.example.testcrud.entity;


import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
public class Genere {
    @Autowired
    @Id
    @GeneratedValue
    public long id;
    public String nomeGenere;

    @OneToMany( mappedBy = "genere")
    private Set<Film> film = new HashSet<>();

}
