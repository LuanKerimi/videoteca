package com.example.testcrud.controller;

import com.example.testcrud.entity.Film;
import com.example.testcrud.entity.Genere;
import com.example.testcrud.entity.User;
import com.example.testcrud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    public UserController(UserService userService){this.userService = userService;};

    @GetMapping("/register")
    public String register(Model model) {
        User newUser = new User();
        model.addAttribute("user", newUser);
        return "user/register";
    }

    @PostMapping("/save")
    public String saveUser(@ModelAttribute("user") User newUser) {
        userService.save(newUser);
        return "redirect:/home";
    }

    @GetMapping("/management")
    public String userManagementHome(Model model){
        Iterable<User> userList = userService.getUsers();
//        String userList = "ciao";
        model.addAttribute("userList",userList);
        return "user/userManagement";
    }

    @GetMapping("/add")
    public String addUser(Model model) {
        User newUser = new User();
        model.addAttribute("user", newUser);
        return "user/register";
    }


//
//    @PostMapping("/save")
//    public String saveUser(@ModelAttribute("user") User newUser, HttpServletRequest req) {
//        userService.save(newUser);
//        return "redirect:/home";
//    }
    @GetMapping("/update")
    public String updateUser(@RequestParam("userId") long userId, Model model) {

        User user = userService.findById(userId);
        model.addAttribute("user", user);
        return "user/createUser";
    }

    @GetMapping("/delete")
    public String deleteUser(@RequestParam("userId") long id){
        userService.deleteById(id);
        return "redirect:/home";
    }


}
