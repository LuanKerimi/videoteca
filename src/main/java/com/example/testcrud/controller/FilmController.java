package com.example.testcrud.controller;

import com.example.testcrud.entity.Film;
import com.example.testcrud.entity.Genere;
import com.example.testcrud.repository.GenereRepository;
import com.example.testcrud.service.FilmService;
import com.example.testcrud.service.GenereService;
import com.example.testcrud.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


@Controller
@RequestMapping("/film")
public class FilmController {

    private static final Logger _log = LogManager.getLogger(FilmController.class.getName());
    @Autowired
    private FilmService filmService;
    @Autowired
    private UserService userService;
    @Autowired
    private GenereService genereService;
    @Autowired
    private GenereRepository genereRepository;


    public FilmController(FilmService filmService) {
        this.filmService = filmService;
    }

    @GetMapping("/create")
    public String filmAdd(Model model) {
        Film newFilm = new Film();
        Iterable<Genere> generiList = genereService.generiList();
        model.addAttribute("film", newFilm);
        model.addAttribute("generiList", generiList);

        return "film/createFilm";
    }

    @PostMapping("/save")
    public String saveFilm(@ModelAttribute("film") Film newFilm, HttpServletRequest req) throws IOException {

        filmService.save(newFilm);
        return "redirect:/home";
    }

    @GetMapping("/update")
    public String updateFilm(@RequestParam("filmId") long filmId, Model model) {

        Film film = filmService.findById(filmId);
        Iterable<Genere> generiList = genereService.generiList();
        model.addAttribute("film", film);
        model.addAttribute("generiList", generiList);

        return "film/createFilm";
    }

    @GetMapping("/delete")
    public String deleteFilm(@RequestParam("filmId") long id) {
        filmService.deleteById(id);

        return "redirect:/home";
    }

    @GetMapping("/catalogo")
    public String sayHello(Model model) {
        Iterable<Film> filmList = filmService.findAll();
        model.addAttribute("filmList", filmList);
        return "film/filmList";
    }

    @GetMapping("/catalogo/taz")
    public String sortAz(Model model) {
        Iterable<Film> filmList = filmService.ordinaAZ();
        model.addAttribute("filmList", filmList);
        return "film/filmList";
    }

    @GetMapping("/catalogo/tza")
    public String sortZA(Model model) {
        Iterable<Film> filmList = filmService.ordinaZA();
        model.addAttribute("filmList", filmList);
        return "film/filmList";
    }

    @GetMapping("/catalogo/aAsc")
    public String sortAnnoAsc(Model model) {
        Iterable<Film> filmList = filmService.ordinaAnnoAsc();
        model.addAttribute("filmList", filmList);
        _log.info("catalogo ASC");
        return "film/filmList";
    }

    @GetMapping("/catalogo/aDesc")
    public String sortAnnoDesc(Model model) {
        Iterable<Film> filmList = filmService.ordinaAnnoDesc();
        model.addAttribute("filmList", filmList);
        return "film/filmList";
    }

}

