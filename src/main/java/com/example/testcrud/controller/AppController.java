package com.example.testcrud.controller;

import com.example.testcrud.entity.Film;
import com.example.testcrud.entity.FilmApi;
import com.example.testcrud.entity.Genere;
import com.example.testcrud.entity.User;
import com.example.testcrud.service.FilmService;
import com.example.testcrud.service.GenereService;
import com.example.testcrud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


@Controller
public class AppController {
    private static final Logger _log = Logger.getLogger(
            AppController.class.getName());
    @Autowired
    UserService userService;
    @Autowired
    GenereService genereService;

    @Autowired
    private FilmService filmService;

    @GetMapping("/home")
    public String showHome(Model model, HttpSession session, @AuthenticationPrincipal User auth) throws IOException {

        //AuthUser

        User authUser = (User) session.getAttribute("authUser");
        model.addAttribute("authUser", authUser);


        if (authUser == null) {
            _log.warning("nessun Auth ");
        } else {
            _log.info("Auth in session");
        }


        //FilmList & Film APILIST
        try {
            Iterable<Film> filmList = filmService.findAll();
            model.addAttribute("filmList", filmList);
            List<FilmApi> filmApiList = filmService.getFilmApi();
            model.addAttribute("filmApiList", filmApiList);
            _log.info("API importata con successo");
        } catch (Exception e) {
            _log.warning("API o filmList mancante");
        }

        ArrayList<Genere> checkGenere = genereService.checkGenere();
        model.addAttribute("checkGenere", checkGenere);

        return "homepage";
    }


    @GetMapping("/")
    public String landing() {
        return "redirect:/home";
    }
}


