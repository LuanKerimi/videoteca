package com.example.testcrud.controller;

import com.example.testcrud.entity.Genere;
import com.example.testcrud.service.GenereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/genere")
public class GenereController {

    @Autowired
    private GenereService genereService;

    public GenereController(GenereService genereService){this.genereService = genereService;};


    @GetMapping("/create")
    public String genereAdd(Model model) {
        Genere newGenere = new Genere();
        model.addAttribute("genere", newGenere);
        return "genere/add";
    }
    @PostMapping("/save")
    public String saveGenere(@ModelAttribute("genere") Genere newGenere) {
        genereService.save(newGenere);
        return "redirect:/home";
    }

    @GetMapping("/management")
    public String genereManagement(Model model){
        Iterable<Genere> generiList = genereService.generiList();
//        String userList = "ciao";
        model.addAttribute("generiList",generiList);
        return "genere/genereManagement";
    }

    @GetMapping("/update")
    public String updatrGenere(@RequestParam("genereId") long genereId, Model model) {

        Genere genere = genereService.findById(genereId);
        model.addAttribute("genere", genere);
        return "genere/add";
    }

    @GetMapping("/delete")
    public String deleteGenere(@RequestParam("genereId") long id){
        genereService.deleteById(id);
        return "redirect:/home";
    }
}
