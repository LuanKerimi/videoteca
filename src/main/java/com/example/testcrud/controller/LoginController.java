package com.example.testcrud.controller;

import com.example.testcrud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @Autowired
    UserService userService;


    @GetMapping("/login")
    public String showMyLogin() {

        return "login/myLogin";
    }

    @GetMapping("/authenticateTheUser")
    public String redirectLogin(@RequestParam("username") String username, @RequestParam("password") String password,
    HttpSession session){

        System.out.println("username");
        System.out.println(session.getAttribute("username"));
        return "redirect:/home";
    }




}

